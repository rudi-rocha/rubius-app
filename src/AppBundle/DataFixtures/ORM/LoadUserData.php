<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    /**
     * Create default users 
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin
            ->setUsername('admin')
            ->setEmail('youremail@admin.dev')
            ->setPlainPassword('123')
            ->setEnabled(true)
        ;

        $manager->persist($userAdmin);
        $manager->flush();
    }
    
}